const express = require('express');
const app = express();
const puerto = 8032;

const carpetapug = app.set('views', './views');
const pug = app.set('view engine', 'pug');

app.get('/', (req, res)=>{
  res.render('index', {orgId: "1", from: "1618286309480", to : "1618307909482", panelId : "4"});
  // res.render('index', {orgId: "1", from: "1618288490853", to : "1618310090853", panelId : "5"});
});

app.listen(puerto, function(){
    console.log("El servidor está activo en http://localhost:8032/");
});